'use strict';

/**
 * @ngdoc overview
 * @name projectxApp
 * @description
 * # projectxApp
 *
 * Main module of the application.
 */
angular
  .module('projectxApp', [
    'ngSanitize',
    'ui.router',
    'ui.bootstrap',
    'uiGmapgoogle-maps',
    'slick'
  ])
  .config(function ($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider) {
    $stateProvider
      .state('index', {
        url: '/',
        templateUrl: 'views/main.html',
        controller: 'HomeCtrl'
      })
      .state('stylis', {
        url: '/stylis',
        templateUrl: 'views/stylis.html',
        controller: 'StylisCtrl'
      })
      .state('search', {
        url: '/search',
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .state('browserPhoto', {
        url: '/browser-photo',
        templateUrl: 'views/browser-photo.html',
        controller: 'BrowsePhoto'
      });

    $urlRouterProvider.otherwise('/');

    uiGmapGoogleMapApiProvider.configure({
      china: true
    });
  });
