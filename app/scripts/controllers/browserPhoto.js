'use strict';

/**
 * @ngdoc function
 * @name projectxApp.controller:BrowsePhoto
 * @description
 * # BrowsePhoto
 * Controller of the projectxApp
 */
angular.module('projectxApp')
  .controller('BrowsePhoto', ['$scope','$uibModal', function($scope, $uibModal) {
    $scope.user = { avatar: 'https://d220aniogakg8b.cloudfront.net/static/uploads/2014/08/13/d7160f94-e74_1660129_100x100.jpg', name: 'Marco Nobre', place: 'Makeup Artist', business: 'Makeup By Ashley', studio: 'My Home Studio', location: 'Columbia, SC 29229'};
    $scope.images = [
      "https://d220aniogakg8b.cloudfront.net/static/uploads/2015/04/02/15a1a610aad1_2223262_300x225.jpg",
      "https://d220aniogakg8b.cloudfront.net/static/uploads/2015/04/02/f9b373d1c49f_2223277_300x225.jpg",
      "https://d220aniogakg8b.cloudfront.net/static/uploads/2015/04/02/ea1c979b8383_2223255_300x225.jpg",
      "https://d220aniogakg8b.cloudfront.net/static/uploads/2015/04/02/936bb24f6583_2223517_300x225.jpg",
      "https://d220aniogakg8b.cloudfront.net/static/uploads/2012/03/15/acd323f02cdb_214650_300x225.jpg",
      "https://d220aniogakg8b.cloudfront.net/static/uploads/2012/03/15/acd323f02cdb_214650_300x225.jpg",
      "https://d220aniogakg8b.cloudfront.net/static/uploads/2015/01/12/fa945185-cef_2021903_300x225.jpg",
      "https://d220aniogakg8b.cloudfront.net/static/uploads/2014/07/28/784de8d43f4f_1618634_300x225.jpg",
      "https://d220aniogakg8b.cloudfront.net/static/uploads/2015/04/01/b1b988b03ab6_2221103_300x225.jpg",
      "https://d220aniogakg8b.cloudfront.net/static/uploads/2015/04/01/e31e75374d4f_2221089_300x225.jpg"
    ];

    $scope.listImages = [];
    // OpenImage Modal
    $scope.openImage = function(index) {
      $uibModal.open({
        templateUrl : 'views/modals/openImage.html',
        controller: 'ModalImageCtrl',
        backdrop : 'static',
        resolve : {
          user: function() {
            return $scope.user;
          },
          index: function() {
            return index;
          },
          images : function() {
            return $scope.images;
          }
        }
      });
    };

    function genarateData() {
      for (var i = 0; i <= 9; i++) {
        $scope.listImages.push({image: $scope.images[i], name: "Jen Atkin", studio: "ANDY LECOMPTE SALON -LA", location: "Los Angeles, CA"});
      }
    }

    genarateData();
  }]);
