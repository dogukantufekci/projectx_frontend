'use strict';

/**
 * @ngdoc function
 * @name projectxApp.controller:SearchCtrl
 * @description
 * # SearchCtrl
 * Controller of the projectxApp
 */
angular.module('projectxApp')
  .controller('SearchCtrl', ['$scope', 'mapService', '$state', function ($scope, mapService, $state) {
    $scope.stores = [];
    $scope.locObj = { latitude: 51.5287336, longitude: -0.382471 };
    $scope.map = { center: $scope.locObj, zoom: 8, bounds: {} , control: {}};
    
    
    $scope.viewMap = function(data) {

    };

    $scope.events = {
      mouseover: function(data) {
        $scope.selectedMarker = data.model;
        $scope.stores[data.key].icon = "images/marker_hover.png";
        $scope.stores[data.key].show = true;
        $scope.map.control.updateModels($scope.stores);
        $scope.$apply();
      },
      mouseout: function(data) {
        $scope.selectedMarker = null;
        $scope.stores[data.key].icon = "images/marker.png";
        $scope.stores[data.key].show = false;
        $scope.map.control.updateModels($scope.stores);
        $scope.$apply();
      },
      click : function(data) {
        $state.go('stylis');
      }
    };

    // Handler when mouse over user
    $scope.mouseOver = function(user) {
      if (_.isEmpty($scope.map.control)) return;
      var user = _.find($scope.stores, {id: user.id});
      user.icon = "images/marker_hover.png";
      $scope.map.control.updateModels($scope.stores);
    };

    // Handler when mouse out user
    $scope.mouseOut = function(user) {
      if (_.isEmpty($scope.map.control)) return;
      var user = _.find($scope.stores, {id: user.id});
      user.icon = "images/marker.png";
      $scope.map.control.updateModels($scope.stores);

    };

    // initialize data
    var markerData = [{longitude: -0.348641, latitude: 51.634985}, {longitude: -0.111926, latitude: 51.509390}, {longitude: 0.186078, latitude: 51.579422}, {longitude: 0.029523, latitude: 51.451232}]
    function randomData() {
      for (var i=0 ; i < 4; i++) {
        var store = { avatar: 'https://d220aniogakg8b.cloudfront.net/static/uploads/2014/08/13/d7160f94-e74_1660129_100x100.jpg', name: 'Name' + i, place: 'Makeup Artist', business: 'Makeup By Ashley', studio: 'My Home Studio', location: 'Columbia, SC 29229', 
        longitude : markerData[i].longitude, latitude: markerData[i].latitude, icon :'images/marker.png', show: false};
        store.id = i;
        store.recommend = i+1;
        $scope.stores.push(store);
      }
    }
    randomData();
  }]);